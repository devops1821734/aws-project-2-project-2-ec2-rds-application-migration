#!/bin/bash
sudo sed -i "/#\$nrconf{restart} = 'i';/s/.*/\$nrconf{restart} = 'a';/" /etc/needrestart/needrestart.conf
sudo apt update
sleep 50
sudo apt install python3-dev -y
sleep 50
sudo apt install python3-pip -y
sleep 50
sudo apt install build-essential libssl-dev libffi-dev -y
sleep 50
sudo apt install libmysqlclient-dev -y
sleep 50
sudo apt install unzip -y
sleep 50
sudo apt install libpq-dev libxml2-dev libxslt1-dev libldap2-dev -y
sleep 50
sudo apt install libsasl2-dev libffi-dev -y
sleep 50
pip install Flask==2.3.3
sleep 50
export PATH=$PATH:/home/ubuntu/.local/bin/
sleep 50
pip3 install wtforms
sleep 50
sudo apt install pkg-config
sleep 50
pip3 install flask_mysqldb
sleep 50
pip3 install passlib
sleep 50
sudo apt-get install mysql-client -y