variable "pub_key_location" {
  sensitive = true
}

variable "private_key_location" {
  sensitive = true
}

variable "region" {
}