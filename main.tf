provider "aws" {
  region = var.region
}

# Defining the VPC
resource "aws_vpc" "production_vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "vpc-bootcamp"
  }
}

# Define the public subnet in us-east-1a
resource "aws_subnet" "public_subnet_us_east_1a" {
  vpc_id                  = aws_vpc.production_vpc.id
  cidr_block              = "10.0.0.0/24"
  availability_zone       = "us-east-1a"
  map_public_ip_on_launch = true # Assign public IPs to instances launched in this subnet
  tags = {
    Name = "Public Subnet us-east-1a"
  }
}

# Define the private subnet in us-east-1a
resource "aws_subnet" "private_subnet_us_east_1a" {
  vpc_id            = aws_vpc.production_vpc.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-1a"
  tags = {
    Name = "Private Subnet us-east-1a"
  }
}

# Define the private subnet in us-east-1b
resource "aws_subnet" "private_subnet_us_east_1b" {
  vpc_id            = aws_vpc.production_vpc.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "us-east-1b"
  tags = {
    Name = "Private Subnet us-east-1b"
  }
}

# Create Internet Gateway
resource "aws_internet_gateway" "igw_mod3" {
  vpc_id = aws_vpc.production_vpc.id
  tags = {
    Name = "igw-mod3"
  }
}

# Get the default route table ID
data "aws_route_tables" "default_route_table" {
  vpc_id = aws_vpc.production_vpc.id

  filter {
    name   = "association.main"
    values = ["true"]
  }
}

# Create a route in the default route table
resource "aws_route" "internet_gateway_route" {
  route_table_id         = data.aws_route_tables.default_route_table.ids[0]
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw_mod3.id
}

# Define the key pair
resource "aws_key_pair" "ec2_ssh" {
  key_name   = "ec2-ssh"
  public_key = file(var.pub_key_location)
}

# Create the EC2 instance
resource "aws_instance" "awsuse1app01" {
  ami                         = "ami-080e1f13689e07408"
  instance_type               = "t2.micro"
  key_name                    = aws_key_pair.ec2_ssh.key_name
  subnet_id                   = aws_subnet.public_subnet_us_east_1a.id
  vpc_security_group_ids      = [aws_security_group.app01_sg.id]
  associate_public_ip_address = true

  tags = {
    Name = "awsuse1app01"
  }

  connection {
    type        = "ssh"
    host        = self.public_ip
    user        = "ubuntu"
    private_key = file(var.private_key_location)
  }

  provisioner "file" {
    source      = "ec2AppScript.sh"
    destination = "/home/ubuntu/ec2AppScript.sh"
  }
  
}

# Create the security group
resource "aws_security_group" "app01_sg" {
  name        = "app01-sg"
  description = "Security group for app01"
  vpc_id      = aws_vpc.production_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "EC2toRDS-sg" {
  name        = "EC2toRDS-sg"
  description = "Security group for RDS instance"
  vpc_id      = aws_vpc.production_vpc.id

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # It's recommended to restrict this to known IPs for security
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "EC2toRDS-sg"
  }
}

resource "aws_db_instance" "awsuse1db01" {
  allocated_storage       = 20 # Minimum allocated storage for MySQL on Free Tier
  storage_type            = "gp2"
  engine                  = "mysql"
  engine_version          = "5.7.44" # Example version, ensure this version is available in your region
  instance_class          = "db.t2.micro"
  db_name                 = "mydb" # DB name
  identifier              = "awsuse1db01"
  username                = "admin"
  password                = "admin123456"
  parameter_group_name    = "default.mysql5.7"
  skip_final_snapshot     = true
  publicly_accessible     = false
  backup_retention_period = 0 # Set according to your backup needs, 0 for disabling automatic backups
  apply_immediately       = true
  availability_zone       = "us-east-1a"
  # VPC Security Group and Subnet group configuration
  # Ensure you replace these with actual IDs from your environment
  vpc_security_group_ids = [aws_security_group.EC2toRDS-sg.id]
  db_subnet_group_name   = aws_db_subnet_group.my_db_subnet_group.name

  # Additional optional configurations
  multi_az          = false
  storage_encrypted = false

  # Tags - optional
  tags = {
    Name = "awsuse1db01"
  }
}

resource "aws_db_subnet_group" "my_db_subnet_group" {
  name       = "my-db-subnet-group" # This is the name you'll reference in your RDS instance
  subnet_ids = [aws_subnet.private_subnet_us_east_1a.id, aws_subnet.private_subnet_us_east_1b.id]
  tags = {
    Name = "My DB Subnet Group"
  }
}
